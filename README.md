# BAP deployment

Local Docker Compose BAP deployment.

It includes:
- Git and Github data sources
- LDAP authentication/authorization
- a script to enable public access and pseudonymization
- a script to add customized panels/menu using the OpenSearch REST APIs

## Getting started

### Requirements

Docker Compose deployment
- Docker
- Docker Compose

Panels and menu script
- bash
- curl
- jq

Security script (Python)
- requests>=2.22.0

### Important settings required

In order to run OpenSearch containers, the `mmap` settings have to be adjusted in the host where they'll run
```
sysctl -w vm.max_map_count=262144
```
To make the change permanent, add the attribute to `/etc/sysctl.conf`
```
vm.max_map_count=262144
```
and reload with
```
sysctl -p
```

Reference:
- https://opensearch.org/docs/latest/opensearch/install/important-settings/
- https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-virtual-memory.html

### Deployment

1. Update any services credentials if needed
2. Set data sources tokens (Github) in `setup.cfg`
3. Run the services with
   ```shell
   docker-compose up -d
   ```
4. Wait for Dashboards to be available at https://localhost
5. Create panels and menu
   ```shell
   ./extra_config/panels/add.sh
   ```
6. Set up anonymous user and pseudonymization OpenSearch roles
   ```shell
   python3 extra_config/security/configure_anonymous.py https://bitergia:changeme@localhost/data
   ```

Service endpoints:
- Dashboards https://localhost
- OpenSearch https://localhost/data
- Hatstall https://localhost/identities
- Hatstall admin UI https://localhost/admin
- LDAP `ldap://localhost:389`
- LDAP admin tool https://localhost:6443

## Data sources configuration

Data sources from where data is collected can be set up in these files
- [projects.json](mordred/conf/projects.json) list the projects collected
- [setup.cfg](mordred/conf/setup.cfg) sets up each data source

Check out the [Grimoirelab tutorial](https://chaoss.github.io/grimoirelab-tutorial/docs/getting-started/configure/) to learn how these files can be configured.

## OpenSearch settings

The main files are
- [opensearch.yml](opensearch/opensearch.yml) OpenSearch cluster and plugins configuration
- [securityconfig/](opensearch/securityconfig/) security plugin configuration: users, roles, authentication/authorization options...
- [opensearch_dashboards.yml](opensearch_dashboards/opensearch_dashboards.yml) OpenSearch Dashboards configuration: branding, visualization options and backend connection configuration

Check out the [OpenSearch documentation](https://opensearch.org/docs/latest) for more info.

## LDAP authentication/authorization

The OpenSearch security plugin configuration is configured to have LDAP authentication and authorization. The initial set of users and groups is in [directory.ldif](ldap/directory.ldif). The users in the `Administrators` and `Developers` groups are mapped to the appropriate OpenSearch roles: `admin`, and `readall/kibanaserver`, respectively.

You can access the administration tool at https://localhost:6443. Acknowledge the security warning and log in using `cn=admin,dc=example,dc=org` and `changethis`.

## Pseudonymization

The [configure_anonymous.py](extra_config/security/configure_anonymous.py) script creates an OpenSearch role called `pseudonymize`. Users with this role will see [personal information fields](extra_config/security/pseudonymize_fields.json) as hashes when this type of information is shown in any dashboard.

This role can be mapped to any user
1. In the left menu click on `Security` (You need to be logged as an admin user)
2. Go to `Roles` and select the `pseudonymize` role
3. Click on the `Mapped users` tab and `Manage mapping`
4. Add any [internal](opensearch/securityconfig/internal_users.yml) or external user in the `Users` field and click `Map`

## Panels and menu

The [panels script](extra_config/panels/add.sh) creates a [default set](extra_config/panels/dashboards_index_patterns.ndjson) of panels and a [menu](extra_config/panels/menu.json) listing them. The script can be re-run with updated panels or menu. Panels can be also imported/exported manually
1. In the left menu click on `Stack Management/Saved Objects`
2. To export any objects to a `.ndjson` file select them on the list and click on `Export`
3. To import an `.ndjson` file, click on `Import` and import the file from your system
