## Roles creation and user mappings migration

Create new roles, tenant, and migrate old user mappings (OpenID) to multi-tenant OpenSearch instance.

Run these scripts:
1. compose_role_users.py
2. create_bap_roles.py
3. map_bap_users.py

### 1. compose_role_users.py

This script compose the roles.json file

```
python3 compose_role_users.py https://admin:admin@localhost:9200 tenant1 roles.json
```
Here the output
```
$ cat roles.json
{
    "bap_tenant1_privileged_user_role": [
        "user1"
    ],
    "all_access": [
        "user_admin"
    ],
    "bap_tenant1_user_role": [
        "user2"
    ],
    "bap_tenant1_mordred_role": [
        "mordred_tenant1"
    ]
}
```

### 2. create_bap_roles.py

This script creates new roles and tenant.

```
python3 create_bap_roles.py https://admin:admin@localhost:9200 tenant1
```

New roles:
- bap_tenant1_privileged_user_role
- bap_tenant1_user_role
- bap_tenant1_mordred_role

New tenant:
- tenant1

### 3. map_bap_users.py

This script map user to the roles defined on roles.json

```
python3 map_bap_users.py https://admin:admin@localhost:9200 roles.json
```
Here the result
```
GET _plugins/_security/api/rolesmapping
{
  "all_access" : {
    "users" : [
      "user_admin"
    ],
    ...
  },
  "bap_tenant1_privileged_user_access" : {
    "users" : [
      "user1"
    ],
    ...
  },
  "bap_tenant1_user_access" : {
    "users" : [
      "user2"
    ],
    ...
  },
  "bap_tenant1_mordred_role" : {
    "users" : [
      "mordred_tenant1"
    ],
    ...
  },
  ...
}
```
