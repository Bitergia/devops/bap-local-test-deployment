#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Angel Jara <ajara@bitergia.com>
#     Quan Zhou <quan@bitergia.com>
#

import argparse
import json

import requests


ROLES_URL = "/_plugins/_security/api/roles/"
ROLESMAPPING_URL = "/_plugins/_security/api/rolesmapping/"

WORKSPACE_PATH = "extra_config/security"
ALIASES_JSON = "mordred/conf/aliases.json"
ANONYMOUS_ROLE_JSON = WORKSPACE_PATH + "/anonymous_role.json"
PSEUDONYMIZE_FIELDS_JSON = WORKSPACE_PATH + "/pseudonymize_fields.json"
PSEUDONYMIZE_ROLE_JSON = WORKSPACE_PATH + "/pseudonymize_role.json"

ANONYMOUS_ROLE_NAME = "anonymous_readall_tenant"
ANONYMOUS_USERNAME = "opendistro_security_anonymous"
PSEUDONYMIZE_ROLE_NAME = "pseudonymize"

ANONYMOUS_ROLES = [ANONYMOUS_ROLE_NAME, "kibana_read_only"]


def main():
    """Creates anonymous user and pseudonymize roles, and maps them to the anonymous user."""

    args = parse_args()
    base_url = args.url
    enriched_aliases = parse_enriched_aliases(read_json(ALIASES_JSON))
    create_anonymous_user_role(base_url, enriched_aliases)
    create_pseudonymize_role(base_url, enriched_aliases, read_json(PSEUDONYMIZE_FIELDS_JSON))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="OpenSearch URL with credentials")
    return parser.parse_args()


def create_anonymous_user_role(base_url, aliases):
    """Creates anonymous user role and maps it to the anonymous user.

    :param base_url: OpenSearch base URL
    :param aliases:  List of aliases to add as readable index patterns to the role
    """

    anonymous_role = read_json(ANONYMOUS_ROLE_JSON)
    index_patterns = aliases + [".kibana"]
    update_index_permissions(anonymous_role, "index_patterns", index_patterns)
    create_role(base_url, ANONYMOUS_ROLE_NAME, anonymous_role)
    for role_name in ANONYMOUS_ROLES:
        map_users_to_role(base_url, [ANONYMOUS_USERNAME], role_name)


def create_pseudonymize_role(base_url, aliases, pseudonymize_fields):
    """Creates pseudonymize role and maps it to the anonymous user.

    :param base_url: OpenSearch base URL
    :param aliases: List of aliases to add as readable index patterns to the role
    :param pseudonymize_fields: Dictionary with masked fields and excluded fields (FLS)
    """

    pseudonymize_role = read_json(PSEUDONYMIZE_ROLE_JSON)
    index_patterns = aliases + [".kibana"]
    update_index_permissions(pseudonymize_role, "index_patterns", index_patterns)
    update_index_permissions(pseudonymize_role, "masked_fields", pseudonymize_fields["masked_fields"])
    update_index_permissions(pseudonymize_role, "fls", pseudonymize_fields["fls"])
    create_role(base_url, PSEUDONYMIZE_ROLE_NAME, pseudonymize_role)
    map_users_to_role(base_url, [ANONYMOUS_USERNAME], PSEUDONYMIZE_ROLE_NAME)


def read_json(path_name):
    with open(path_name, 'r') as fd:
        return json.load(fd)


def parse_enriched_aliases(aliases):
    """Parse standard Grimoirelab aliases file to obtain all enriched aliases

    :param aliases: Grimoirelab aliases

    :return: Unique sorted list of enriched aliases
    """

    enriched_aliases = []
    for alias in aliases:
        for phase in aliases[alias]:
            if phase != "enrich":
                continue
            enriched_aliases += [enrich["alias"] for enrich in aliases[alias][phase]]
    return sorted(list(set(enriched_aliases)))


def update_index_permissions(role, key, value):
    role["index_permissions"][0][key] = value


def create_role(base_url, role_name, role):
    """Create an OpenSearch role based on a template.

    :param base_url: OpenSearch base URL
    :param role_name: Name of the role to create
    :param role: Role template
    """

    url = base_url + ROLES_URL + role_name
    run_request(url, body=role, request_type="put")


def run_request(url, body=None, request_type="get"):
    """Run an API REST request.

    :param url: Resource URL
    :param body: Body contents
    :param request_type: put | post | patch | get (default)

    :return: Request response
    """

    headers = {'Content-type': 'application/json'}
    verify = False
    if request_type == "put":
        r = requests.put(url, data=json.dumps(body), verify=verify, headers=headers)
    elif request_type == "post":
        r = requests.post(url, data=json.dumps(body), verify=verify, headers=headers)
    elif request_type == "patch":
        r = requests.patch(url, data=json.dumps(body), verify=verify, headers=headers)
    elif request_type == "get":
        r = requests.get(url, verify=verify, headers=headers)
    else:
        raise ValueError("Invalid request: {}".format(request_type))
    r.raise_for_status()
    return r


def get_role_mappings(base_url, role_name):
    """Obtain a list of users mapped to an OpenSearch role.

    :param base_url: OpenSearch base URL
    :param role_name: Name of the role

    :return: List of users | None
    """

    url = base_url + ROLESMAPPING_URL + role_name
    try:
        reply = run_request(url)
        return reply.json()[role_name]["users"]
    except requests.exceptions.HTTPError as e:
        if e.response.status_code != 404:
            raise requests.exceptions.HTTPError(e)
        return None


def map_users_to_role(base_url, users, role_name):
    """Map a list of users to an OpenSearch role.

    :param base_url: OpenSearch base URL
    :param users: List of users to map
    :param role_name: Name of the role
    """

    role_mappings = get_role_mappings(base_url, role_name)
    url = base_url + ROLESMAPPING_URL + role_name
    if not role_mappings:
        body = {"users": users}
        run_request(url, body=body, request_type="put")
    else:
        body = [{
            "op": "add",
            "path": "/users",
            "value": list(set(role_mappings + users))
        }]
        run_request(url, body=body, request_type="patch")


if __name__ == '__main__':
    main()
