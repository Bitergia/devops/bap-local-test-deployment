#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json

import requests


API_ROLESMAPPING = "/_plugins/_security/api/rolesmapping/"

BAP_MORDRED_ROLE = "bap_{}_mordred_role"
BAP_PRIVILEGED_USER_ROLE = "bap_{}_privileged_user_role"
BAP_USER_ROLE = "bap_{}_user_role"


def main():
    args = args_parser()
    opensearch_url = args.opensearch_url
    tenant = args.tenant
    output = args.output

    base_url = opensearch_url + API_ROLESMAPPING
    response = run_requests("GET", base_url)

    role_users = extract_role_users(response)
    new_role_users = compose_roles_with_tenant(role_users, tenant)

    write_json(new_role_users, output)


def args_parser():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("opensearch_url",
                        help="OpenSearch URL")
    parser.add_argument("tenant",
                        help="Name of the tenant")
    parser.add_argument("output",
                        help="Name of the output file")
    args = parser.parse_args()

    return args


def run_requests(method, url, data=None, verify=False):
    headers = {'Content-type': 'application/json'}
    if method == "GET":
        r = requests.get(url, verify=verify, headers=headers)
    elif method == "POST":
        r = requests.post(url, data=data, verify=verify, headers=headers)
    elif method == "PUT":
        r = requests.put(url, data=data, verify=verify, headers=headers)
    elif method == "PATCH":
        r = requests.patch(url, data=data, verify=verify, headers=headers)

    r.raise_for_status()
    return r.json()


def extract_role_users(result_json):
    roles = {}
    for role in result_json:
        if not result_json[role]['users']:
            continue
        roles[role] = result_json[role]['users']

    return roles


def compose_roles_with_tenant(role_users, tenant):
    def add_user(roles, role_name, user):
        if role_name not in roles:
            roles[role_name] = []
        if user not in roles[role_name]:
            roles[role_name].append(user)

    all_users = [role_users[rol] for rol in role_users]
    flat_users = [users for users_list in all_users for users in users_list]

    roles = {}
    for user in flat_users:
        if user in role_users["all_access"]:
            role_name = "all_access"
            add_user(roles, role_name, user)
        if user in role_users["readall"] and user not in role_users["kibana_user"]:
            role_name = BAP_USER_ROLE.format(tenant)
            add_user(roles, role_name, user)
        if user in role_users["readall"] and user in role_users["kibana_user"]:
            role_name = BAP_PRIVILEGED_USER_ROLE.format(tenant)
            add_user(roles, role_name, user)
    mordred_role = BAP_MORDRED_ROLE.format(tenant)
    user = "mordred_{}".format(tenant)
    add_user(roles, mordred_role, user)

    return roles


def write_json(data, file_name):
    with open(file_name, 'w') as f:
        f.write(json.dumps(data, indent=4))


if __name__ == '__main__':
    main()
