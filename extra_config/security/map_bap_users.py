#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json

import requests


API_ROLESMAPPING = "/_plugins/_security/api/rolesmapping/"


def main():
    args = args_parser()
    opensearch_url = args.opensearch_url
    roles_file = args.roles

    roles = read_json(roles_file)

    for role in roles:
        base_url = opensearch_url + API_ROLESMAPPING + role

        if role == "all_access":
            response = update_rolemapping(base_url, roles[role])
        else:
            response = create_rolemapping(base_url, roles[role])
        print("{}: {}".format(response['status'], response['message']))


def create_rolemapping(base_url, users):
    data = {
        "users": users
    }
    response = run_requests("PUT", base_url, data=json.dumps(data))
    return response


def update_rolemapping(base_url, value):
    data = [{
        "op": "add", "path": "/users", "value": value
    }]
    response = run_requests("PATCH", base_url, data=json.dumps(data))
    return response


def args_parser():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("opensearch_url",
                        help="OpenSearch URL")
    parser.add_argument("roles",
                        help="roles JSON file")
    args = parser.parse_args()

    return args


def read_json(file_name):
    with open(file_name, "r") as f:
        return json.loads(f.read())


def run_requests(method, url, data=None, verify=False):
    headers = {'Content-type': 'application/json'}
    if method == "GET":
        r = requests.get(url, verify=verify, headers=headers)
    elif method == "POST":
        r = requests.post(url, data=data, verify=verify, headers=headers)
    elif method == "PUT":
        r = requests.put(url, data=data, verify=verify, headers=headers)
    elif method == "PATCH":
        r = requests.patch(url, data=data, verify=verify, headers=headers)

    r.raise_for_status()
    return r.json()


if __name__ == '__main__':
    main()
