#!/bin/bash

set -euo pipefail

: ${OPENSEARCH_URL:=https://localhost/data}
: ${OPENSEARCH_DASHBOARDS_URL:=https://localhost}
: ${OPENSEARCH_USERNAME:=bitergia}
: ${OPENSEARCH_PASSWORD:=changeme}
: ${DASHBOARDS_FILE:=extra_config/panels/dashboards_index_patterns.ndjson}
: ${MENU_FILE:=extra_config/panels/menu.json}

echo -n "Setting dynamic mapping: "
curl -XPUT -k -s \
    -u "${OPENSEARCH_USERNAME}:${OPENSEARCH_PASSWORD}" \
    "${OPENSEARCH_URL}/.kibana/_mapping" \
    -H 'Content-Type: application/json' \
    -d @- << EOF | tee /dev/tty \
    | jq -e '.acknowledged == true' >/dev/null
{
  "dynamic": true
}
EOF

echo
echo -n "Creating menu: "
curl -XPUT -k -s \
    -u "${OPENSEARCH_USERNAME}:${OPENSEARCH_PASSWORD}" \
    "${OPENSEARCH_URL}/.kibana/_doc/menu" \
    -H 'Content-Type: application/json' \
    -d @"${MENU_FILE}" | tee /dev/tty \
    | jq -e '.result == "created" or .result == "updated"' >/dev/null

echo
echo -n "Importing dashboards and index patterns to OpenSearch Dashboards: "
curl -XPOST -k -s \
    -u "${OPENSEARCH_USERNAME}:${OPENSEARCH_PASSWORD}" \
    "${OPENSEARCH_DASHBOARDS_URL}/api/saved_objects/_import?overwrite=true" \
    -H "osd-xsrf: true" \
    --form file=@"${DASHBOARDS_FILE}" \
    | jq -e '.success == true' > /dev/null && echo OK || echo ERROR
