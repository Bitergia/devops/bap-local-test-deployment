#!/bin/bash

set -exuo pipefail

: ${CONTAINER_NAME:=opensearch-node1}
: ${CA_CERT:=root-ca.pem}
: ${ADMIN_CERT:=admin.pem}
: ${ADMIN_KEY:=admin-key.pem}


docker exec ${CONTAINER_NAME} \
bash plugins/opensearch-security/tools/securityadmin.sh \
-cd plugins/opensearch-security/securityconfig/ \
-cacert config/${CA_CERT} \
-cert config/${ADMIN_CERT} \
-key config/${ADMIN_KEY} \
-icl -nhnv